myDirCY ='\\hest.nas.ethz.ch\green_groups_fi_public\1 Group Organization\3 Alumni\0.2 ARCHIVE\Patricia Burkhardt_2023\GG022_001\Zstack\new_protocol\3-CY5';
myCY = dir(fullfile(myDirCY,'*.tif'));

name_cellcy = {myCY.name}; 
tname_cellcy=name_cellcy';
vec=[];
vec_I3=[];
vec_I3_2=[];
vec_thresh=[];
delta=[];
max_value=[];
mean_value=[];


for k = 1:length(myCY) 
    filename = myCY(k).name; 
    fullfilename=fullfile(myDirCY,filename); 
    imageCFP=imread(fullfilename);
    if numel(size(imageCFP))== 2
        I=imageCFP;
    else
        I=rgb2gray(imageCFP);
    end
    I = uint8(255*mat2gray(I));
    I2=I;
    I=medfilt2(I); 
    I = imadjust(I);
    I=imhmax(I,40,4);
    I = imtophat(I, strel('disk', 22));

I3=I;
    meanI=mean(I3);
    x=sum(meanI);
    mean_overall= x/length(I3);
    maxI=max(I3);
    y=sum(maxI);
    max_overall=y/length(I3);
    d=max_overall - mean_overall;
    max_value(k)=max_overall;
    mean_value(k)=mean_overall;
    delta(k)=d;
    I3(I3>max_overall)=0;
    level_I3 = graythresh(I3);%compute threshold with removed outlier, very high peaks
    vec_I3(k)= level_I3; 
  
    level = graythresh(I);%compute threshold with removed outlier, very high peaks
    vec(k)= level;
    if round(level,3)  >= 0.26
        level=0.28;
    elseif round(level,3)  > 0.26 &&round(level-level_I3,3)>=0.005
        level=0.35;
    else 
        level=0.22;
    end
    bw1=imbinarize(I,level);
    bw1 = bwareaopen(bw1, 40);
    vec_I3_2(k)= level;
    
    
    %single cell area
    bw1_s=bw1;
    cc_s=bwconncomp(bw1_s,4); 
    stats_s=regionprops(cc_s,'all');
    threshold_s=150;
    solidthresh_s=0.8;
    remove=[stats_s.Area]>threshold_s;
    remove1=[stats_s.Solidity]<solidthresh_s;
    bw1_s(cat(1,cc_s.PixelIdxList{remove}))=false;
    bw1_s(cat(1,cc_s.PixelIdxList{remove1}))=false;
    bw1_s=bwareaopen(bw1_s,20,4);
   
    %dense cell area
    bw1_c=bw1-bw1_s;
    
    %remove long shaped food
    cc_c=bwconncomp(bw1_c,4);
    stats_l = regionprops('table',cc_c,'BoundingBox','Area','MajorAxisLength','MinorAxisLength','SubarrayIdx');
    stats_l.LenWdRatio =  stats_l.MajorAxisLength ./ stats_l.MinorAxisLength;
    thresh_l = 4.5; 
    stats_l.isGreater = stats_l.LenWdRatio < thresh_l; 
    objRemoveIdx = find(~stats_l.isGreater); 
    for i = find(~stats_l.isGreater).'
        bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
    end
    %remove noise resp to object area/Boundingbox area
    TF=isempty(stats_l);
    if TF == 0
        stats_l.BBArea=stats_l.BoundingBox(:,3).*stats_l.BoundingBox(:,4);
        if stats_l.BBArea <1022976 %outlier condition
            stats_l.AreaRatio=stats_l.Area./stats_l.BBArea;
            AreaTh=0.15;
            stats_l.noise = stats_l.AreaRatio>AreaTh;
            objRemoveIdx = find(~stats_l.noise); 
            for i = find(~stats_l.noise).'
                bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
            end
        end
    end
    
    
    
    
    
    %clustered cells detection
    M = bsxfun(@times, I2, cast(bw1_c, 'like', I2));
    M = localcontrast(M, 0.5, 0.8);
    
    

    edges=edge(M,'Canny',[0.00000001,0.06]);
    bridge=bwmorph(edges,'bridge');
    diluted=imdilate(bridge,true(3));
    eroded=imerode(diluted,true(3));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    diluted=imdilate(eroded,true(3));
    eroded=imerode(diluted,true(3));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    filled=imfill(eroded,'holes');
    bw1_c=filled-eroded;
    bw1_c=bwareaopen(bw1_c,20,4);
    
        
    bw1_se=bw1_c;
    cc_se=bwconncomp(bw1_se,4); 
    stats_se=regionprops(cc_se,'all');
    threshold_se=150;
    solidthresh_se=0.8;
    remove=[stats_se.Area]>threshold_se;
    remove1=[stats_se.Solidity]<solidthresh_se;
    bw1_se(cat(1,cc_se.PixelIdxList{remove}))=false;
    bw1_se(cat(1,cc_se.PixelIdxList{remove1}))=false;
    bw1_se=bwareaopen(bw1_se,20,4);
    
    
    
    %dense cells extraction #2
    bw1_c=bw1_c-bw1_se;
    
    M = bsxfun(@times, I, cast(bw1_c, 'like', I));
    M = localcontrast(M, 0.4, 0.7);
    
    
    edges=edge(M,'Canny',[0.0000001,0.06]);
    bridge=bwmorph(edges,'bridge');
    diluted=imdilate(bridge,true(3));
    eroded=imerode(diluted,true(3));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    diluted=imdilate(eroded,true(3));
    eroded=imerode(diluted,true(3));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    filled=imfill(eroded,'holes');
    bw1_c=filled-eroded;
    bw1_c=bwareaopen(bw1_c,20,4);
    
    cc_c=bwconncomp(bw1_c,4); 
    stats_c=regionprops(cc_c);
    
    
    
    
    
    %total cell area
    bw1_t=bw1_s+bw1_se+bw1_c;
    bw1_t=imbinarize(bw1_t);
    bw1_t=bwareaopen(bw1_t,4,4);
    er_t=bwconncomp(bw1_t,4);
    imagedata_total=regionprops(er_t,'basic');
    
     
    cfp_t_areas=cat(1,imagedata_total.Area); 
    cfp_t_xy=cat(1,imagedata_total.Centroid); 
    CFP_total=[cfp_t_areas,cfp_t_xy]; 

    num_ob_t=er_t.NumObjects; 
    area_pix_t=sum([imagedata_total.Area]);
    mt_t(k,:)=[area_pix_t num_ob_t];
    
    
    %save binary image total
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\3_Cy5\binary';
    imwrite(bw1_t,fullfile(folder,sprintf('%s.tif',filename)));
    %Save area and their coordinates matrix total
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\3_Cy5\coordinates';
    save(fullfile(folder,sprintf('%s.txt',filename)),'CFP_total','-ascii');
    
    overlay1=imoverlay(I2,bw1_t,[0,1,0]); 
    %save binary image 1000
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\3_Cy5\overlay';
    imwrite(overlay1,fullfile(folder,sprintf('%s.tif',filename)));
end
mean_level=sum(vec)/length(vec); 
vec=vec.'
vec_I3=vec_I3.'
delta=delta.'
max_value=max_value.'
mean_value=mean_value.'
vec_I3_2=vec_I3_2.'

finalfile=cat(2,tname_cellcy,num2cell(mt_t));
finaltable=cell2table(finalfile,'VariableNames',{'file','area','ob_num'});
folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\3_Cy5';
writetable(finaltable,fullfile(folder,'Btheta_coordinates_longterm_total_stitched.txt'));