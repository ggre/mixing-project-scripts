%read grayscale original raw microscope images
myDir ='\\hest.nas.ethz.ch\green_groups_fi_public\1 Group Organization\3 Alumni\0.2 ARCHIVE\Patricia Burkhardt_2023\GG022_001\Zstack\new_protocol\1-CFP';
myCFP = dir(fullfile(myDir,'*.tif')); 
name_cell = {myCFP.name}; 
name_cell2=erase(name_cell, '.tif');
tname_name=name_cell'; 
vec=[];
vec_I3=[];
vec_I3_2=[];
delta=[];
max_value=[];
mean_value=[];

for k = 1:length(myCFP) 
    filename = myCFP(k).name; 
    fullfilename=fullfile(myDir,filename); 
    imageCFP=imread(fullfilename);
    if numel(size(imageCFP))== 2
        I=imageCFP;
    else
        I=rgb2gray(imageCFP);
    end
    I = uint8(255*mat2gray(I));
    I2=I;
    I=medfilt2(I); 
    I = imadjust(I);
    I=imhmax(I,20,4);
    I = imtophat(I, strel('disk', 7));

I3=I;
    meanI=mean(I3);
    x=sum(meanI);
    mean_overall= x/length(I3);
    maxI=max(I3);
    y=sum(maxI);
    max_overall=y/length(I3);
    d=max_overall - mean_overall;
    max_value(k)=max_overall;
    mean_value(k)=mean_overall;
    delta(k)=d;
    I3(I3>max_overall)=0;
    level_I3 = graythresh(I3);%compute threshold with removed outlier, very high peaks
    vec_I3(k)= level_I3; 
  
    level = graythresh(I);%compute threshold with removed outlier, very high peaks
    vec(k)= level;
    if round(level,3)  > 0.20 && round(level-level_I3,3)<0.03
       level=0.3;
    elseif round(level,3)  > 0.20 &&round(level-level_I3,3)>=0.03
        level=0.41;
    else 
        level=0.26;
    end
    bw1=imbinarize(I,level);
    bw1 = bwareaopen(bw1, 60);
    vec_I3_2(k)= level;
    
    %single cell area
    bw1_s=bw1;
    cc_s=bwconncomp(bw1_s,4); 
    stats_s=regionprops(cc_s,'all','Circularity');
    threshold_s=400;
    solidthresh_s=0.7;
    %minExtent=0.9;
    maxEccentricity = 0.99;
    remove=[stats_s.Area]>threshold_s;
    remove1=[stats_s.Solidity]<solidthresh_s;
    %remove2=[stats_s.Circularity]<minExtent;
    remove3=[stats_s.Eccentricity]>maxEccentricity;
    bw1_s(cat(1,cc_s.PixelIdxList{remove}))=false;
    bw1_s(cat(1,cc_s.PixelIdxList{remove1}))=false;
    %bw1_s(cat(1,cc_s.PixelIdxList{remove2}))=false;
    bw1_s(cat(1,cc_s.PixelIdxList{remove3}))=false;
    bw1_s=bwareaopen(bw1_s,30);

 
    %dense cell area
    bw1_c=bw1-bw1_s;
    
    %remove long shaped objects
    cc_c=bwconncomp(bw1_c,4);
    stats_l = regionprops('table',cc_c,'BoundingBox','Area','MajorAxisLength','MinorAxisLength','SubarrayIdx');
    stats_l.LenWdRatio =  stats_l.MajorAxisLength ./ stats_l.MinorAxisLength;
    thresh_l = 4; 
    stats_l.isGreater = stats_l.LenWdRatio < thresh_l; 
    objRemoveIdx = find(~stats_l.isGreater); 
    for i = find(~stats_l.isGreater).'
        bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
    end
    %remove noise resp to object area/Boundingbox area
    TF=isempty(stats_l);
    if TF == 0
        stats_l.BBArea=stats_l.BoundingBox(:,3).*stats_l.BoundingBox(:,4);
        if stats_l.BBArea <1022976 %outlier condition
            stats_l.AreaRatio=stats_l.Area./stats_l.BBArea;
            AreaTh=0.15;
            stats_l.noise = stats_l.AreaRatio>AreaTh;
            objRemoveIdx = find(~stats_l.noise); 
            for i = find(~stats_l.noise).'
                bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
            end
        end
    end
    
    bw1_c=imfill(bw1_c);
    cc_c=bwconncomp(bw1_c,4); 
    stats_c=regionprops(cc_c,'all','Circularity');
    solidthresh_c=0.7;
    threshold_s=100;
    remove1=find([stats_c.Solidity]<solidthresh_c & [stats_c.Area]<threshold_s);
    %remove2=[stats_c.Circularity]<minExtent;
    bw1_c(cat(1,cc_c.PixelIdxList{remove1}))=false;
    %bw1_c(cat(1,cc_c.PixelIdxList{remove2}))=false;
    bw1_c=bwareaopen(bw1_c,50);
    bw1_c=imdilate(bw1_c,true(4));
   
    
    
    M = bsxfun(@times, I2, cast(bw1_c, 'like', I2));
    M = localcontrast(M, 0.5, 0.8);
    bw1_m=bw1_c;
    

    edges=edge(M,'Canny',[0.000001,0.06]);
    bridge=bwmorph(edges,'bridge');
    diluted=imdilate(bridge,true(2));
    eroded=imerode(diluted,true(2));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    diluted=imdilate(eroded,true(1));
    eroded=imerode(diluted,true(1));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    filled=imfill(eroded,'holes');
    bw1_c=filled-eroded;
    bw1_c=bwareaopen(bw1_c,20);
    
        
    bw1_se=bw1_c;
    cc_se=bwconncomp(bw1_se,4); 
    stats_se=regionprops(cc_se,'all');
    threshold_se=150;
    solidthresh_se=0.5;
    remove=[stats_se.Area]>threshold_se;
    remove1=[stats_se.Solidity]<solidthresh_se;
    bw1_se(cat(1,cc_se.PixelIdxList{remove}))=false;
    bw1_se(cat(1,cc_se.PixelIdxList{remove1}))=false;
    bw1_se=bwareaopen(bw1_se,30,4);
    
    
    
    %dense cells extraction #2
    bw1_c=bw1_c-bw1_se;
    bw1_m=bw1_c-bw1_m;
    bw1_m=imbinarize(bw1_m,0.5);
    bw1_c=bw1_c-bw1_m;
    bw1_c=imbinarize(bw1_c,0.5);
    bw1_c=bwareaopen(bw1_c,50,4);
    bw1_m=bw1_c;
    
    M = bsxfun(@times, I, cast(bw1_c, 'like', I));
    M = localcontrast(M, 0.4, 0.7);
    
    
    edges=edge(M,'Canny',[0.000001,0.06]);
    bridge=bwmorph(edges,'bridge');
    diluted=imdilate(bridge,true(2));
    eroded=imerode(diluted,true(2));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    diluted=imdilate(eroded,true(1));
    eroded=imerode(diluted,true(1));
    eroded=bwmorph(eroded,'thin');
    eroded=bwmorph(eroded,'bridge');
    filled=imfill(eroded,'holes');
    bw1_c=filled-eroded;
    bw1_c=bwareaopen(bw1_c,20,4);
    bw1_m=bw1_c-bw1_m;
    bw1_m=imbinarize(bw1_m,0.5);
    bw1_c=bw1_c-bw1_m;
    bw1_c=imbinarize(bw1_c,0.5);
    bw1_c=bwareaopen(bw1_c,20,4);
    
    
    %remove long shaped objects
    cc_c=bwconncomp(bw1_c,4);
    stats_l = regionprops('table',cc_c,'BoundingBox','Area','MajorAxisLength','MinorAxisLength','SubarrayIdx');
    stats_l.LenWdRatio =  stats_l.MajorAxisLength ./ stats_l.MinorAxisLength;
    thresh_l = 4; 
    stats_l.isGreater = stats_l.LenWdRatio < thresh_l; 
    objRemoveIdx = find(~stats_l.isGreater); 
    for i = find(~stats_l.isGreater).'
        bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
    end
    %remove noise resp to object area/Boundingbox area
    TF=isempty(stats_l);
    if TF == 0
        stats_l.BBArea=stats_l.BoundingBox(:,3).*stats_l.BoundingBox(:,4);
        if stats_l.BBArea <1022976 %outlier condition
            stats_l.AreaRatio=stats_l.Area./stats_l.BBArea;
            AreaTh=0.2;
            stats_l.noise = stats_l.AreaRatio>AreaTh;
            objRemoveIdx = find(~stats_l.noise); 
            for i = find(~stats_l.noise).'
                bw1_c(stats_l.SubarrayIdx{i,1},stats_l.SubarrayIdx{i,2}) = false; 
            end
        end
    end
   
    
    
    bw1_c=bw1_se+bw1_c;
    cc_c=bwconncomp(bw1_c,4); 
    stats_c=regionprops(cc_c);
    
    
    
    %total cell area
    bw1_t=bw1_s+bw1_c;
    bw1_t=imbinarize(bw1_t);
    bw1_t=bwareaopen(bw1_t,4,4);
    er_t=bwconncomp(bw1_t,4);
    imagedata_total=regionprops(er_t,'basic');
    
     
    cfp_t_areas=cat(1,imagedata_total.Area); 
    cfp_t_xy=cat(1,imagedata_total.Centroid); 
    CFP_total=[cfp_t_areas,cfp_t_xy]; 

    num_ob_t=er_t.NumObjects; 
    area_pix_t=sum([imagedata_total.Area]);
    mt_t(k,:)=[area_pix_t num_ob_t];
    
    
    %save binary image total
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\1_CFP\binary';
    imwrite(bw1_t,fullfile(folder,sprintf('%s.tif',filename)));
    %Save area and their coordinates matrix total
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\1_CFP\coordinates';
    save(fullfile(folder,sprintf('%s.txt',filename)),'CFP_total','-ascii');
    
    overlay1=imoverlay(I2,bw1_c, [1 .3 .3]);
    overlay1=imoverlay(overlay1,bw1_s, [0,1,0]);
    %save binary image 1000
    folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\1_CFP\overlay';
    imwrite(overlay1,fullfile(folder,sprintf('%s.tif',filename)));
end


mean_level=sum(vec)/length(vec); 
vec=vec.'
vec_I3=vec_I3.'
vec_I3_2=vec_I3_2.'
delta=delta.'
max_value=max_value.'
mean_value=mean_value.'

finalfile=cat(2,tname_name,num2cell(mt_t));
finaltable_t=cell2table(finalfile,'VariableNames',{'file','area','ob_num'});
folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2022\GG022_001\Matlab_output\1_CFP';
writetable(finaltable_t,fullfile(folder,'Ecoli_coordinates_longterm_total.txt'));
