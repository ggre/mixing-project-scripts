setBatchMode(true);

// Set the directory path where your files are located
dir = getDirectory("Choose a Directory");

// Set the output directory path where you want to save the individual channels
outputDir = getDirectory("Choose a Directory"); // Update with the desired output directory

// Get a list of all files in the directory
list = getFileList(dir);

// Load the Bio-Formats plugin
run("Bio-Formats Macro Extensions");

// Loop through each file in the list
for (i = 0; i < list.length; i++) {
    fileName = list[i];
    filePath = dir + fileName;

    // Check if the file has the .ids extension
    if (endsWith(fileName, ".ids")) {
        // Open the file using Bio-Formats
        run("Bio-Formats", "open=[" + filePath + "] color_mode=Default rois_import=[ROI manager] split_channels view=Hyperstack stack_order=XYCZT");

        // Get the number of channels
        nChannels = 3;

        // Loop through each channel
        for (c = 0; c < nChannels; c++) {
            // Select the channel
            selectWindow(fileName+ " - C=" + c);

            // Z-projection
            run("Z Project...", "projection=[Max Intensity]");

            // Save the z-projection image to the output directory
            saveAs("Tiff", outputDir + File.nameWithoutExtension + "_Channel" + c + "_zprojection.tif");

            // Close the projection image
            close();
        }

        // Close the opened image
        close();
    }
}

setBatchMode(false);