%channel 1
myDir1 ='\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2023\GG023_012\Zstack';
myCh1 = dir(fullfile(myDir1,'*.tif'));
name_cell1 = {myCh1.name};
tname_cell1=name_cell1'; 
vec1=[];


for k = 1:length(myCh1)
    %channel 1
    filename1 = myCh1(k).name; 
    fullfilename1=fullfile(myDir1,filename1); 
    image1=imread(fullfilename1);
    if numel(size(image1))== 2
        I1=image1;
    else
        I1=rgb2gray(image1);
    end
   
    level1 = graythresh(I1);
    vec1(k)= level1;
    bw1=imbinarize(I1,0.1);
    bw1 = bwareaopen(bw1, 20);
    se=strel('disk',2,8);
    bw1=imerode(bw1,se);
    %se=strel('disk',1,8);
    %bw1=imdilate(bw1, se);
    %component analysis
    er1=bwconncomp(bw1);
    imagedata1=regionprops(er1,'basic'); 
    ch1_area=cat(1,imagedata1.Area);  
    ch1_xy=cat(1,imagedata1.Centroid);
    ch1_total=[ch1_area,ch1_xy]; 
    num_ob_1=er1.NumObjects; 
    pix1=sum([imagedata1.Area]);
    mt_1(k,:)=[pix1 num_ob_1];
    
    %save binary image
    folder1 = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2023\GG023_012\Matlab_Output\Binary';
    imwrite(bw1,fullfile(folder1,sprintf('%s.tif',filename1)));
    %Save coordinates
    folder1 = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2023\GG023_012\Matlab_Output\Coordinates';
    save(fullfile(folder1,sprintf('%s.txt',filename1)),'ch1_total','-ascii');
    %save Overlay
    overlay1=imoverlay(image1,bw1, [0,1,1]);
    folder1 = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2023\GG023_012\Matlab_Output\Overlay';
    imwrite(overlay1,fullfile(folder1,sprintf('%s.tif',filename1)));
    
end  
vec1=vec1.'


finalfile=cat(2,tname_cell1,num2cell(mt_1));
finaltable1=cell2table(finalfile,'VariableNames',{'file','area','ob_num'});
folder = '\\hest.nas.ethz.ch\green_groups_fi_public\Giorgia Greter\Experiments\2023\GG023_012\Matlab_Output';
writetable(finaltable1,fullfile(folder,'Beads_Channel_1_stitched.txt'));
